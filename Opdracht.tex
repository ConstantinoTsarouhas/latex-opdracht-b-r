% LaTeX report by Constantino Tsarouhas, 'met Nederlandstalige' comments

% Document-klasse
\documentclass[a4paper]{article}

% Pakketten en configuraties
\usepackage{amsmath,amssymb,amsthm}			% Wiskundige ondersteuning
\usepackage[dutch]{babel}					% Nederlandstalig document
\usepackage[utf8]{inputenc}					% Invoer in UTF-8 (Unicode)
\usepackage[T1]{fontenc}					% Interne codering in T1
\usepackage{graphicx, wrapfig}				% Figuren
\usepackage{hyperref}						% Interactieve URL's
\usepackage{dirtytalk}						% Automatische aanhalingstekens
\usepackage{epigraph}						% Leuk citaat
\usepackage[superscript,biblabel]{cite}		% Mooiere referentie-verwijzingen in tekst
\usepackage{svg}							% SVG-ondersteuning

% Document-metadata
\title{\LaTeX-opdracht Bewijzen \& Redeneren}
\author{Constantino Tsarouhas (r0369382)}
\date{\today}

% Speciale wiskundige functies
\DeclareMathOperator{\Ai}{Ai}
\DeclareMathOperator{\Bi}{Bi}

% Soorten stellingen
\theoremstyle{definition}
\newtheorem{property}{Eigenschap}[section]
\newtheorem{definition}{Definitie}[section]
\newtheorem{theorem}{Stelling}[section]

% Einde van preamble-omgeving
% Begin van document-omgeving
\begin{document}

\maketitle

\section{Over~George~Biddell~Airy}

\epigraph{In the hands of Science and indomitable energy, results the most gigantic and absorbing may be wrought out by skilful combinations of acknowledged data and the simplest means.}{Sir~George~Biddell~Airy\cite{quote}}

\begin{wrapfigure}{r}{0.4\textwidth}
	\begin{center}
		\includegraphics[width=3cm]{Airy1891}
	\end{center}
	\caption{Airy in 1891}
	\caption*{\footnotesize Bron: \url{https://commons.wikimedia.org/wiki/File:George_Biddell_Airy_1891.jpg}}
	\label{img:airy}
\end{wrapfigure}

Sir~George~Biddell~Airy (cf. figuur~\ref{img:airy}) is geboren in Alnwick (Northumberland, Engeland) op 27~juli~1801. Hij kwam van een lange lijn van Airy's wiens voorouders met zelfde familienaam in de 14de eeuw in Kentmere (Westmorland, Engeland) leefden. De genealogische lijn waartoe Sir Airy behoorde behoorde later — wegens een reeks van burgeroorlogen — tot Lincolnshire, waar men als landbouwers leefden.\cite{enc}

De kleine George~Airy werd eerst onderwezen in basisscholen in Hereford, later in de Colchester~Grammar~School. In 1819 begon hij in Trinity~College te Cambridge als een \emph{sizar}, een student dat maaltijden en andere accomodaties krijgt in ruil voor een vooraf bepaalde job. Daar had hij een briljante carrière en werd hij vrijwel onmiddellijk erkend als leidende man van het jaar. In 1822 was hij verkozen geleerde van Trinity, het daaropvolgend jaar kreeg hij zijn eerste prijs, \emph{Smith's~prize}, en eind 1826 werd hij professor in de wiskunde. Deze positie hield hij echter slechts voor een jaar; tot zijn benoeming als professor in de astronomie alsook de nieuwe directeur van het observatorium van Cambridge. Tussen 1835 en 1881 was hij \emph{Astronomer~Royal} in de \emph{Royal~Observatory} in Greenwich.\cite{bio, enc}

Over de jaren heen schreef Airy 11 boeken, waaronder \emph{Trigonometry}~(1825), \emph{Gravitation}~(1834) en \emph{Partial~differential~equations}~(1866) alsook meer dan 500 papers en rapporten, dankzij zijn hard werk en efficiënte werkmethode. Daarnaast leverde hij grote bijdragen aan de wiskunde en astronomie: hij verbeterde de theorie rond de banen van Venus en de maan, bestudeerde regenbogen op een wiskundige wijze en berekende de massadichtheid van de Aarde. Hij kreeg daardoor een rij prijzen alsook topposities in organisaties zoals de \emph{Royal~Astronomical~Society}~(1845), \emph{British~Association}~(1851), \emph{Royal~Society~of~London}~(1871) en \emph{Institut~de~France}~(1872).\cite{bio}

Buiten zijn wetenschappelijke disciplines, had hij interesse voor poëzie, geschiedenis, antiek, architectuur, theologie, geologie en bouwkunde. Hij publiceerde zelfs papers voor zijn andere interesses, zoals de exacte locatie waar Julius Caesar in Groot-Brittannië aankwam en vanwaar hij vertrok.\cite{bio}

Airy overleed op 2 januari 1892 op 90-jarige leeftijd ten gevolge van inwendige verwondingen na een valpartij. Hij liet 9 kinderen na, waarvan 6 hem overleefden.

\section{De Airyfunctie}

De eenvoudige differentiaalvergelijking
\begin{equation*}
	\frac{d^2y}{dx^2} - y = 0
\end{equation*}
heeft algemene oplossing $ y = c_1 \sinh(x) + c_2 \cosh(x) $. Sir~George~Biddell~Airy bestudeerde in 1838 een vergelijkbare vergelijking,
\begin{equation}
	\label{eq:prob}
	\frac{d^2y}{dx^2} - xy = 0.
\end{equation}
Airy stelde twee partiële oplossingen $ y_1 $ en $ y_2 $ voor (\ref{eq:prob}) in de vorm van een machtreeks $ y(x) = \sum_{i = 0}^{\infty} a_i x^i $, die door J.~C.~P.~Miller in 1946 de \emph{Airyfuncties} $ \Ai $ en $ \Bi $ werden genoemd.\cite{wolfram}

De Airyfuncties
\begin{align*}
	\Ai(x) & = \frac{1}{\pi}\int_0^\infty\cos\left(\tfrac{t^3}{3} + xt\right)\, dt \text{ en} \\
	\Bi(x) & = \frac{1}{\pi} \int_0^\infty \left[\exp\left(-\tfrac{t^3}{3} + xt\right) + \sin\left(\tfrac{t^3}{3} + xt\right)\,\right]dt
\end{align*}
zijn de oplossingen van de differentiaalvergelijking (\ref{eq:prob}).\cite{wolfram} Voor (\ref{eq:prob}) geldt namelijk
\begin{equation*}
	y(x) = c_1 \Ai(\sqrt[3]{-1} x) + c_2 \Bi(\sqrt[3]{-1} x).
\end{equation*}

Een eerste eigenschap dat volgt uit de definitie is dat $ \Ai $ nergens een complexe waarde geeft als men uitsluitend met reële getallen werkt. Hetzelfde geldt voor haar afgeleide.\cite{wolfram}

\begin{property}
	\label{prop:closure}
	Voor elke $ x \in \mathbb{R} $ geldt $ \Ai(x) \in \mathbb{R} $ en $ \Ai'(x) \in \mathbb{R} $.
\end{property}

Een tweede eigenschap is dat de Airyfuncties exponentieel gedragen voor positieve waarden en periodisch voor negatieve waarden.\cite{phys} Zie ook figuur~\ref{fig:plot}.
\begin{property}
	Voor $ x \to \infty $ geldt
	\begin{align*}
		\Ai(x) & \sim \frac{1}{2\sqrt{\pi}} x^{-1/4} \exp{\left(-\frac{2}{3} x^{3/2}\right)} \text{ en} \\
		\Ai(-x) & \sim \frac{1}{\sqrt{\pi}} x^{-1/4} \cos{\left(\frac{2}{3} x^{3/2} - \frac{\pi}{4}\right)}.
	\end{align*}
\end{property}

\begin{figure}
	\begin{center}
		\includegraphics[width=\textwidth]{AiryPlot}
	\end{center}
	\caption{De Airyfuncties $ \Ai $ en $ \Bi $}
	\caption*{\footnotesize Merk het periodisch gedrag voor negatieve waarden en het exponentieel gedrag voor positieve waarden.}
	\caption*{\footnotesize Bron: \mbox{\url{https://commons.wikimedia.org/wiki/File:Airy_Functions.svg}}}
	\label{fig:plot}
\end{figure}

\section{Een determinant met Airyfuncties}

In een recent proefschrift rond het numeriek benaderen van integralen komen zekere stelsels lineaire vergelijkingen voor waarvan de coëfficiënten uitgedrukt kunnen worden in de Airyfunctie en haar afgeleiden. Het oplosbaar zijn van het stelsel komt overeen met het niet-nul zijn van een determinant van Airyfuncties, gegeven in definitie~\ref{def:det}.

\begin{definition}
	\label{def:det}
	De \textbf{determinantsfunctie van Airyfuncties} van orde $ n $ is
	\begin{equation*}
		D_n(x) = \det \left[\Ai^{(j+k-2)}(x)\right]^n_{j,k=1}.
	\end{equation*}
	Voor $ n = 0 $ wordt de functie gedefiniëerd als
	\begin{equation*}
		D_0(x) = 1.
	\end{equation*}
\end{definition}

In het proefschrift wordt bewezen dat voor even waarden van $ n $ geldt dat $ D_n(x) \neq 0 $ voor reële waarden van $ x $. Voor oneven waarden van $ n $ zijn er oneindig veel reële nulpunten die allemaal strikt negatief zijn. Bijgevolg geldt de volgende stelling.

\begin{theorem}
	\label{th:Droots}
	Voor elke even $ n \in \mathbb{N} $ geldt dat $ D_n $ geen reële nulpunten heeft.
\end{theorem}

\begin{proof}
	We zullen de stelling bewijzen voor het geval $ n = 2 $.
	
	Veronderstel dat $ x $ een reëel getal is. Dan volgt uit definitie~\ref{def:det} dat
	\begin{equation*}
		D_2(x) = \det
		\begin{bmatrix}
			\Ai(x) & \Ai'(x) \\
			\Ai'(x) & \Ai''(x)
		\end{bmatrix}
		= \Ai(x)\Ai''(x) - \Ai'(x)^2.
	\end{equation*}
	
	Door gebruik te maken van de differentiaalvergelijking~(\ref{eq:prob}) kunnen we $ D_2 $ ook schrijven als
	\begin{equation}
		\label{eq:D2}
		D_2(x) = x \Ai(x)^2 - \Ai'(x)^2.
	\end{equation}
	
	De afgeleide van $ D_2(x) $ kunnen we vinden door eerst elke term van $ D_2 $ uit (\ref{eq:D2}) te differentiëren. Dit leidt dus tot
	\begin{equation}
		\label{eq:diff1}
		D'_2(x) = \frac{d}{dx}\left(x \Ai(x)^2\right) - \frac{d}{dx}\left(\Ai'(x)^2\right).
	\end{equation}
	
	Door de productregel op de eerste term in het rechterlid van (\ref{eq:diff1}) toe te passen krijgen we
	\begin{equation}
		\label{eq:diff2}
		D'_2(x) = \left[ \Ai(x)^2 + x \left(\frac{d \Ai(x)^2}{dx} \right) \right] - \frac{d \Ai'(x)^2}{dx}.
	\end{equation}
		
	De afgeleide $ \frac{d \Ai(x)^2}{dx} $ in (\ref{eq:diff2}) kan met de kettingregel $ \frac{d}{dx} f\left(g(x)\right) = f'\left(g(x)\right) g'(x) $ gevonden worden. Neem $ f(x) = x^2 $ en $ g(x) = \Ai(x) $ zodat $ f'(x) = 2x $ en $ g'(x) = \Ai'(x) $. Uit de kettingregel volgt dan
	\begin{equation}
		\label{eq:diff3}
		\frac{d \Ai(x)^2}{dx} = 2 \Ai(x) \Ai'(x).
	\end{equation}
	
	Door (\ref{eq:diff3}) in (\ref{eq:diff2}) te substitueren krijgen we
	\begin{equation}
		\label{eq:diff4}
		D'_2(x) = \Ai(x)^2 + x \left[2 \Ai(x) \Ai'(x) \right] - \frac{d \Ai'(x)^2}{dx}.
	\end{equation}
	
	De afgeleide $ \frac{d \Ai'(x)^2}{dx} $ in (\ref{eq:diff4}) kan analoog met de kettingregel gevonden worden. Neem $ f(x) = x^2 $ en $ g(x) = \Ai'(x) $ zodat $ f'(x) = 2x $ en $ g'(x) = \Ai''(x) $. Uit de kettingregel volgt dan
	\begin{equation}
		\label{eq:diff5}
		\frac{d \Ai'(x)^2}{dx} = 2 \Ai'(x) \Ai''(x).
	\end{equation}
	
	Door (\ref{eq:diff5}) in (\ref{eq:diff4}) te substitueren krijgen we
	\begin{equation}
		\label{eq:diff6}
		D'_2(x) = \Ai(x)^2 + 2x \Ai(x) \Ai'(x) - \left[2 \Ai'(x) \Ai''(x)\right].
	\end{equation}
	
	Aangezien de Airyfunctie voldoet aan de differentiaalvergelijking (\ref{eq:prob}), geldt dat $ \Ai''(x) = x \Ai(x) $. Door deze substitutie in (\ref{eq:diff6}) uit te voeren volgt
	\begin{equation}
		\label{eq:diff7}
		D'_2(x) = \Ai(x)^2 + 2x \Ai(x) \Ai'(x) - \left[2x \Ai(x) \Ai'(x)\right].
	\end{equation}
	
	Daar de twee laatste termen elkaar opheffen vereenvoudigt (\ref{eq:diff7}) zich tot
	\begin{equation}
		\label{eq:diffA}
		D'_2(x) = \Ai(x)^2.
	\end{equation}
	
	Sinds uit definitie~\ref{def:det} volgt dat $ D_1(x) = \Ai(x) $, geldt na substitutie in (\ref{eq:diffA})
	\begin{equation}
		\label{eq:diffB}
		D'_2(x) = D_1(x)^2.
	\end{equation}
	
	Doordat $ x $ reëel is, is $ \Ai(x) $ ook reëel, vanwege eigenschap~\ref{prop:closure}. Het kwadraat van een reëel getal is altijd positief, waardoor $ D'_2(x) $ in (\ref{eq:diffA}) positief en reëel is. Daar $ D'_2 $ het afgeleide is van $ D_2 $ en dat afgeleide nooit negatief is voor elke $ x $, geldt dat $ D_2 $ een monotoon stijgende functie is.
	
	Aangezien $ \Ai(x) $ naar 0 gaat als $ x \to \infty $, gaat $ D'_2(x) $ ook naar 0 als $ x \to \infty $, vanwege (\ref{eq:diffA}). Dit betekent dat $ D_2(x) $ convergeert naar een waarde als $ x \to \infty $. Omdat $ \Ai(x) $ en $ \Ai'(x) $ in (\ref{eq:D2}) beiden naar 0 gaan als $ x \to \infty $, is deze waarde ook 0. Met andere woorden,
	\begin{equation}
		\label{eq:limit}
		\lim_{x \to \infty} D_2(x) = 0.
	\end{equation}
	
	Een monotoon stijgende functie zoals $ D_2 $ dat naar 0 gaat op oneindig is noodzakelijkerwijs negatief voor elke $ x $. Een functie dat negatief is voor elke $ x $ kan onmogelijk nulpunten hebben. Hiermee is stelling~\ref{th:Droots} bewezen voor het geval $ n = 2 $.
	
\end{proof}

% TODO: Stelling 1 en bewijs voor n = 2

\begin{thebibliography}{99}
	
	\bibitem{quote}
		Sir George Biddell Airy (1855).
		\emph{Lecture on the pendulum-experiments at Harton Pit: delivered in the Central Hall, South Shields, October 24, 1854}.
		Longman~and~Co.~p.~iv.
	
	\bibitem{bio}
		O'Connor, J. J. \& Robertson, E. F. (2001).
		\emph{George Biddell Airy}.
		Online op \mbox{\url{http://www-history.mcs.st-andrews.ac.uk/Biographies/Airy.html}}, bezocht op 18 november 2015.
	
	\bibitem{enc}
		\emph{Encyclopaedia Britannica}.
		Cambridge University Press (1911), 29 vols.
		\say{Airy, Sir George Biddell}, p.~445–447.
		Online exemplaar op \mbox{\url{https://archive.org/details/encyclopaediabri01chisrich}}, bezocht op 18 november 2015.
	
	\bibitem{wolfram}
		Wolfram Research, Inc.
		\emph{Introduction to the Airy functions}.
		Online op \url{http://functions.wolfram.com/Bessel-TypeFunctions/AiryAi/introductions/Airys/ShowAll.html}, bezocht op 20 november 2015.
	
	\bibitem{phys}
		N.~N.~Lebedev~(1855).
		\emph{Special Functions and Their Applications}.
		Dover~Publications~Inc.,~Mineola,~NY.
		Via \mbox{\url{http://scipp.ucsc.edu/~haber/ph216/airy12.ps}}, bezocht op 20 november 2015.
	
\end{thebibliography}

\end{document}